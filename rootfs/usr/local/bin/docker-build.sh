#! /bin/bash
set -euxo pipefail

function load_suffix() {
  local suffix=""
  local arch=$(apk --print-arch)

  case "${arch}" in
    x86_64)
      suffix="amd64"
      ;;
    aarch64)
      suffix="arm64"
      ;;
    armv7)
      suffix="arm-v7"
      ;;
    armhf)
      suffix="arm-v6"
      ;;
    ppc64le)
      suffix="ppc64le"
      ;;
    s390x)
      suffix="s390x"
      ;;
    *)
      suffix="${arch}"
      ;;
  esac

  echo "${suffix}"
}

function load_version() {
  local version="${DOCKER_BUILDX_VERSION:-}"

  if [[ "" == "${version}" ]]; then
    version=$(curl -s https://api.github.com/repos/docker/buildx/releases/latest | jq -r '.tag_name')
  fi

  echo "${version}"
}

function download_package() {
  local bin=/usr/bin

  apk add --no-cache bash curl jq lua5.3
  ln -sf "${bin}"/lua5.3 "${bin}"/lua
  ln -sf "${bin}"/luac5.3 "${bin}"/luac
}

function download_buildx() {
  local version=$(load_version)
  local folder="${HOME}/.docker/cli-plugins"
  local source="https://github.com/docker/buildx/releases/download/${version}/buildx-${version}.linux-$(load_suffix)"
  local target="${folder}/docker-buildx"

  mkdir -p "${folder}"
  wget -qO "${target}" "${source}"
  chmod +x "${target}"
}

function download_binfmt() {
  local prefix=${1:-}
  local folder="${prefix}/usr/sbin"
  local source="https://raw.githubusercontent.com/qemu/qemu/master/scripts/qemu-binfmt-conf.sh"
  local target="${folder}/qemu-binfmt-conf.sh"

  if [[ ! -f "${target}" ]]; then
    mkdir -p "${folder}"
    wget -qO "${target}" "${source}"
  fi

  chmod +x "${target}"
}

function build() {
  # apk add --no-cache docker iproute2 git
  apk add --no-cache docker-cli qemu-alpha qemu-arm qemu-armeb qemu-sparc qemu-sparc32plus qemu-sparc64 qemu-ppc qemu-ppc64 qemu-ppc64le qemu-m68k qemu-mips qemu-mipsel qemu-mipsn32 qemu-mipsn32el qemu-mips64 qemu-mips64el qemu-sh4 qemu-sh4eb qemu-s390x qemu-aarch64 qemu-aarch64_be qemu-hppa qemu-riscv32 qemu-riscv64 qemu-xtensa qemu-xtensaeb qemu-microblaze qemu-microblazeel qemu-or1k
  download_package
  download_buildx
  download_binfmt
}

function main() {
  local call="${1:-}"

  if [[ -z $(typeset -F "${call}") ]]; then
    return
  fi

  shift
  ${call} "$@"
}

main "$@"
