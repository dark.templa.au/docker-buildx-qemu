FROM docker.io/library/alpine
COPY rootfs /

RUN set +e -ux \
  && chmod +x /usr/local/bin/* \
  && apk add --no-cache bash lua5.3 \
  && ln -sf /usr/bin/lua5.3 /usr/bin/lua  \
  && ln -sf /usr/bin/luac5.3 /usr/bin/luac  \
  && /usr/local/bin/docker-build.sh build
# && /usr/local/bin/docker-build.lua
