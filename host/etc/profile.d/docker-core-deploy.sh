#! /bin/bash
set +eux

function main() {
  local deploy="/home/core/deploy"
  local profile="${deploy}/profile.sh"
  export PATH="${PATH}:${deploy}"
  export LUA_PATH="${deploy}/lib/?.lua;;"
  export DOCKER_CORE_DEPLOY="${deploy}"

  [[ -d "${deploy}" ]] && chmod +x "${deploy}"/*
  [[ -f "${profile}" ]] && source "${profile}"
}

main
unset main
set +eux
