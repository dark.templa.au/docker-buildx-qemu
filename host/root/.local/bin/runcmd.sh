#! /bin/bash
set +eux

function main() {
  groupmod -g 500 core
  usermod -u 500 core
  usermod -g 500 core
  chown -R core:core /home/core
  usermod -aG docker core
  [[ -x /usr/sbin/grubby ]] && grubby --update-kernel=ALL --args="systemd.unified_cgroup_hierarchy=0"
  [[ -r /etc/default/grub ]] && sed -i "s%^GRUB_CMDLINE_LINUX=.*$%GRUB_CMDLINE_LINUX=\"cgroup_enable=memory swapaccount=1\"%" /etc/default/grub && update-grub
  # [[ -r /usr/lib/systemd/system/docker.service ]] && sed -ri "s%^ExecStart=([^ ]+).*$%ExecStart=\1 -H unix:///var/run/docker.sock -H fd:// \$DOCKER_OPTS%" /usr/lib/systemd/system/docker.service && systemctl daemon-reload && systemctl restart docker.service && systemctl enable --now docker.service
  # curl -sfL https://get.k3s.io | sh -
}

main
unset main
set +eux
