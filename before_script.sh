#! /bin/sh
set -eux

function main() {
  local rootfs="${PWD}/rootfs"
  local deploy="${PWD}/deploy"
  local bin="${rootfs}/usr/local/bin"
  local run="${bin}/docker-build.sh"

  apk add --no-cache bash
  chmod +x "${bin}"/*.sh
  ${run} download_package
  ${run} download_binfmt "${rootfs}"

  export LUA_PATH="${deploy}/lib/?.lua;;"
  export DOCKER_BUILDX_VERSION="$(${run} load_version)"
}

main "$@"
