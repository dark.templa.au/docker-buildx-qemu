# Summary

Source: https://gitlab.com/agrozyme-docker/docker-buildx-qemu

Alpine-based image with docker client, buildx, and qemu. Easily allows cross platform image builds.

# Description

Inspired By [jonoh/docker-buildx-qemu](https://hub.docker.com/r/jonoh/docker-buildx-qemu)

It's been tested with GitLab CI on gitlab.com, but it should work anywhere that docker-in-docker already works, and with a binfmt_misc enabled kernel.

# Target Platforms

The Docker image can `run` in the environment (The intersection of Alpine and Docker Buildx support platforms):

- linux/amd64
- linux/arm64
- linux/arm/v7
- linux/arm/v6
- linux/ppc64le
- linux/s390x

# Support Platforms

This Docker image can `emulate` the environments:

- linux/amd64
- linux/386
- linux/arm64
- linux/arm/v7
- linux/arm/v6
- linux/ppc64le
- linux/s390x

# GitLab CI Template

It has two template files for building multi-platform Docker images.

- Single: For a single branch repository, deploy `master` to `latest`
- Multiple: Each branch is mapped to a docker image tag and the repository name is the same as Docker Registry.

## Docker Hub

- Single: `/.gitlab/ci_templates/single/docker-buildx.docker-hub.yml`
- Multiple: `/.gitlab/ci_templates/multiple/docker-buildx.docker-hub.yml`

## GitLab Container Registry

- Single: `/.gitlab/ci_templates/single/docker-buildx.gitlab.yml`
- Multiple: `/.gitlab/ci_templates/multiple/docker-buildx.gitlab.yml`

# Workflow:

- The `master` tag of the docker image is used to test the build.
- The `build` job pushes the `master` tag of the docker image.
- If the `build` job is successful, then the `delpoy` job pushes the multi-platform Docker image.
- After pushing the docker image, the `delpoy` job will update the `description` of the Docker Hub repository.
  - Short description: The `description` of the [project](https://gitlab.com/help/user/project/settings/index.md#general-settings).
  - Full description: `README.md`

# Environment variables

Put all docker image repositories into a `Group` and set [Variables](https://gitlab.com/help/ci/variables/README#group-level-environment-variables) in `CI / CD Settings`

## Override priority

Put the variables in `.gitlab-ci.yml` or `CI / CD Settings` to override default.

- `.gitlab-ci.yml` stage variables
- `.gitlab-ci.yml` global variables
- Project `CI / CD Settings`
- Group `CI / CD Settings`

## Required

**No default value, all variables are required. (Only for Docker Hub)**

- `DOCKER_HUB_USER`: Login username
- `DOCKER_HUB_PASS`: Login password encoded by `base64`
- `DOCKER_HUB_NAMESPACE`:
  - username or organization
  - It will be passed to the `Dockerfile` as an `ARG`.

## Optional

- `DOCKER_HUB_API_URL`
  - The Docker Registry API url
  - Only use `.gitlab-ci.yml` to override
  - default: `https://hub.docker.com/v2`
- `DOCKER_REGISTRY`
  - The `docker login` server
  - Only use `.gitlab-ci.yml` to override
  - It will be passed to the `Dockerfile` as an `ARG`.
  - default: `docker.io`
- `DOCKER_PLATFORM`
  - The target platform for building docker image.
  - default: `linux/amd64,linux/386,linux/arm64,linux/arm/v7`.

# Hook Scripts

It has two `bash` scripts to hook `before_script` and `after_script`, the scripts run at beginning and end of the docker build and push action.

- before_script.sh
- after_script.sh

Note:

- If you want to download the files during the build of docker image, you can do this by writing `before_script.sh` and using `COPY` command in the `Dockerfile`.
- It is just done once before / after the `docker buildx` and you can speed up the build of multi-platform docker image.

# Example

This GitLab example should give you an idea of how to use the image.

.gitlab-ci.yml

```yml
include:
  - project: 'agrozyme-docker/docker-buildx-qemu'
    ref: master
    file: '/.gitlab/ci_templates/single/docker-buildx.docker-hub.yml'
```
