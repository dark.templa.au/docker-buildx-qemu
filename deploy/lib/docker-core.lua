local M = {}

---@param text string
---@param code string
function M.color(text, code)
  local escape = string.char(27)
  local prefix = escape .. '[' .. code .. 'm'
  local suffix = escape .. '[0m'
  return prefix .. text .. suffix
end

---@param formatText string
function M.error(formatText, ...)
  local code = '31'
  local text = formatText:format(...)
  print(M.color(text, code))
end

---@param formatText string
function M.warn(formatText, ...)
  local code = '33'
  local text = formatText:format(...)
  print(M.color(text, code))
end

---@param formatText string
function M.info(formatText, ...)
  local code = '32'
  local text = formatText:format(...)
  print(M.color(text, code))
end

---@param command function
function M.explodeList(command, ...)
  ---@type string[]
  local items = {}
  local data = command(...) .. '\n'

  for item in data:gmatch('([^\r\n]+)[\r\n]+') do
    items[#items + 1] = item
  end

  return items
end

---@param formatText string
function M.captureRaw(formatText, ...)
  local command = formatText:format(...)
  ---@type file
  local file = assert(io.popen(command, 'r'))
  ---@type string
  local text = assert(file:read('*a'))
  file:close()
  return text
end

---@param formatText string
function M.captureList(formatText, ...)
  return M.explodeList(M.captureRaw, formatText, ...)
end

---@param formatText string
function M.capture(formatText, ...)
  local items = M.captureList(formatText, ...)
  return table.concat(items, ' ')
end

---@param command function
function M.captureCommand(command, ...)
  local file = os.tmpname()
  local parameter = M.join(' ', ...) .. ' > ' .. file
  command(parameter)

  ---@type string[]
  local items = M.readList(file)
  M.execute('rm -f %s', file)
  return items
end

---@param formatText string
function M.test(formatText, ...)
  local command = 'test ' .. formatText:format(...)
  return os.execute(command)
end

---@param formatText string
function M.realpath(formatText, ...)
  local command = 'realpath -m ' .. formatText:format(...)
  return M.capture(command)
end

---@param path string
function M.dirname(path)
  return M.capture('dirname ' .. path)
end

---@param path string
---@param suffix string
function M.basename(path, suffix)
  suffix = suffix or ''
  local command = ('basename %s %s'):format(path, suffix)
  return M.capture(command)
end

---@param formatText string
function M.execute(formatText, ...)
  local command = formatText:format(...)
  local _, text = assert(os.execute(command))
  return text
end

---@param formatText string
function M.run(formatText, ...)
  local command = formatText:format(...)
  M.info('+ %s', command)
  return M.execute(formatText, ...)
end

---@param name string
---@param default string
function M.getEnvironment(name, default)
  return os.getenv(name) or default
end

---@param items table<string,string>
function M.getEnvironmentTable(items)
  ---@type table<string,string>
  local result = {}

  for index, item in pairs(items) do
    result[index] = M.getEnvironment(index, item)
  end

  return result
end

function M.currentPath()
  return os.getenv('PWD')
end

function M.homePath()
  return os.getenv('HOME')
end

function M.deployPath()
  return os.getenv('DOCKER_CORE_DEPLOY')
end

function M.stackPath()
  return M.deployPath() .. '/stack'
end

function M.dataPath()
  return M.deployPath() .. '/data'
end

function M.updateUser()
  local default = { DOCKER_CORE_UID = '500', DOCKER_CORE_GID = '500' }
  ---@type {DOCKER_CORE_UID: string, DOCKER_CORE_GID: string}
  local item = M.getEnvironmentTable(default)
  os.execute('deluser core')
  os.execute('delgroup core')
  M.execute('addgroup --system --gid %s core', item.DOCKER_CORE_GID)
  M.execute(
    'adduser --system --disabled-password --no-create-home --ingroup=core --gecos=core --shell=/bin/bash --uid=%s core',
    item.DOCKER_CORE_UID
  )
end

function M.chown(...)
  for _, item in pairs({ ... }) do
    if (M.test('-e %s', item)) then
      M.execute('chown -R core:core %s', item)
    end
  end
end

function M.mkdir(...)
  for _, item in pairs({ ... }) do
    if (M.test('! -e %s', item)) then
      M.execute('mkdir -p %s', item)
    end
  end

  M.chown(...)
end

function M.clearPath(...)
  for _, item in pairs({ ... }) do
    if (M.test('-e %s', item)) then
      M.execute('rm -rf %s', item)
    end
  end

  M.mkdir(...)
end

---@param folder string
---@param file string
---@param doCompress boolean
function M.tar(folder, file, doCompress)
  file = file or ''
  folder = folder or ''

  if ('' == file) or ('' == folder) then
    return
  end

  if doCompress then
    if M.test('! -d %s', folder) then
      return
    end

    M.execute('tar -zcf %s -C %s .', file, folder)
  else
    if M.test('! -f %s', file) then
      return
    end

    M.clearPath(folder)
    M.execute('tar -zxf %s -C %s .', file, folder)
  end

end

---@param basePath string
---@param relativePath string
function M.pairPaths(basePath, relativePath)
  relativePath = relativePath or ''

  ---@type {inside: string, outside: string}
  local items = {}
  local paths = { inside = basePath, outside = M.currentPath() }

  for key, value in pairs(paths) do
    items[key] = M.realpath(value .. '/' .. relativePath)
  end

  return items
end

function M.prepareRun(...)
  M.updateUser()
  M.mkdir(...)
end

---@param path string
function M.readFile(path)
  ---@type file
  local file = assert(io.open(path, 'r'))
  ---@type string
  local text = file:read('a')
  file:close()
  return text
end

---@param path string
function M.readList(path)
  return M.explodeList(M.readFile, path)
end

---@param path string
function M.writeFile(path, ...)
  ---@type file
  local file = assert(io.open(path, 'w'))
  file:write(...)
  file:flush()
  return file:close()
end

---@param path string
function M.appendFile(path, ...)
  ---@type file
  local file = assert(io.open(path, 'a'))
  file:write(...)
  file:flush()
  return file:close()
end

---@param requires table<string, string>
function M.requires(requires)
  ---@type table<string, any>
  local items = {}

  for index, item in pairs(requires) do
    items[index] = require(item)
  end

  return items
end

---@param requires table<string, string>
---@param updates table<string, function>
function M.replaceFiles(requires, updates)
  local state, items = pcall(M.requires, requires)

  for target, update in pairs(updates) do
    if (state) then
      M.info('> update: %s', target)
      update(target, items)
    else
      local source = M.dirname(target) .. '/.' .. M.basename(target)
      M.run('mv -f %s %s', source, target)
    end
  end
end

function M.hasModules(...)
  local items = { ... }
  local handler = function()
    for _, item in pairs(items) do
      require(item)
    end
  end

  return pcall(handler)
end

---@param stdout string
---@param stderr string
function M.linkLog(stdout, stderr)
  local command = 'ln -sf /dev/%s %s'

  if (stdout) then
    M.run(command, 'stdout', stdout)
  end

  if (stderr) then
    M.run(command, 'stderr', stderr)
  end
end

---@param text string
function M.trim(text)
  return text:match('^()%s*$') and '' or text:match('^%s*(.*%S)')
end

---@param item any
function M.boolean(item)
  local type = type(item)
  local switch = {
    ['nil'] = function(_)
      return false
    end,
    ---@param value number
    number = function(value)
      return (0 ~= value)
    end,
    ---@param value string
    string = function(value)
      local allowed = { TRUE = true, YES = true, ON = true }
      local text = M.trim(value):upper()

      if (allowed[text]) then
        return true
      else
        return false
      end
    end,
    ---@param value boolean
    boolean = function(value)
      return value
    end
  }

  if (switch[type]) then
    return switch[type](item)
  else
    return true
  end
end

function M.getSource()
  local source = debug.getinfo(2, 'S').source:sub(2)
  return M.capture('realpath ' .. source)
end

---@param glue string
function M.join(glue, ...)
  return table.concat({ ... }, glue)
end

---@param items table
function M.tableCount(items)
  local count = 0

  for _, _ in pairs(items) do
    count = count + 1
  end

  return count
end

---@param items table
function M.isArray(items)
  local count = M.tableCount(items)

  for index = 1, count do
    if nil == items[index] then
      return false, count
    end
  end

  return true, count
end

---@param items table
function M.tableKeys(items)
  local data = {}

  if ('table' ~= type(items)) then
    return data
  end

  for key, _ in pairs(items) do
    data[#data + 1] = key
  end

  return data
end

---@param items table
function M.tableValues(items)
  local data = {}

  if ('table' ~= type(items)) then
    return data
  end

  for _, value in pairs(items) do
    data[#data + 1] = value
  end

  return data
end

---@param items table
function M.tableKeyedKeys(items)
  local data = {}

  if ('table' ~= type(items)) then
    return data
  end

  for key, _ in pairs(items) do
    data[key] = key
  end

  return data
end

---@param items table
function M.tableKeyedValues(items)
  local data = {}

  if ('table' ~= type(items)) then
    return data
  end

  for _, value in pairs(items) do
    data[value] = value
  end

  return data
end

function M.tableMerge(...)
  local items = {}

  for _, item in pairs({ ... }) do
    if ('table' == type(item)) then
      for key, value in pairs(item) do
        items[key] = value
      end
    end
  end

  return items
end

---@param item any
function M.dump(item)
  local invoke = {}

  ---@param text string
  function invoke.quote(text)
    local replace = {
      ['\t'] = '\\t',
      ['\r'] = '\\r',
      ['\n'] = '\\n',
      ['\"'] = '\\"',
      ['\\'] = '\\\\',
    }
    return '"' .. text:gsub('[%c\\"]', replace) .. '"'
  end

  ---@param level number
  function invoke.getIndent(level)
    return ('  '):rep(level)
  end

  ---@param key any
  function invoke.wrapKey(key)
    local case = type(key)
    local formatText = '[%s]'

    if 'number' == case then
      return formatText:format(key)
    elseif 'string' == case then
      return formatText:format(invoke.quote(key))
    else
      return formatText:format(tostring(key))
    end
  end

  ---@param value any
  ---@param level number
  function invoke.wrapValue(value, level)
    local case = type(value)

    if 'table' == case then
      return invoke.dumpTable(value, level)
    elseif 'number' == case then
      return value
    elseif 'string' == case then
      return invoke.quote(value)
    else
      return tostring(value)
    end
  end

  ---@param items any
  ---@param level number
  function invoke.dumpTable(items, level)
    if 'table' ~= type(items) then
      return invoke.wrapValue(items)
    end

    level = level + 1
    local indent = invoke.getIndent(level)
    local isArray = M.isArray(items)
    local data = {}
    data[#data + 1] = '{'

    for key, value in pairs(items) do
      local wrapItem = invoke.wrapValue(value, level) .. ','

      if (false == isArray) then
        wrapItem = invoke.wrapKey(key) .. ' = ' .. wrapItem
      end

      data[#data + 1] = indent .. wrapItem
    end

    data[#data + 1] = invoke.getIndent(level - 1) .. '}'
    return table.concat(data, '\n')
  end

  ---@type string
  local data = invoke.dumpTable(item, 0)
  M.info(data)
  return data
end

return M
